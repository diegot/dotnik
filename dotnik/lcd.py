import serial
from time import sleep 

class Lcd(object):
    lcd = None
    screen_width = 16
    lines = 2

    def __init__(self, port='/dev/ttyAMA0', baudrate=9600):
        # Initialize serial connection
        self.lcd = serial.Serial(port=port, baudrate=baudrate)
        self._set_brightness()
        self.clear()
        sleep(.5)

    def clear(self):
        self.lcd.write(chr(254))
        self.lcd.write(chr(1))

    def display_line1(self, text):
        self._set_position(0)
        self.lcd.write(text)

    def display_line2(self, text):
        self._set_position(16)
        self.lcd.write(text)

    def display(self, text):
        self.clear()
        if len(text) > self.screen_width:
            self.display_line1(text[:16])
            self.display_line2(text[16:32])
        else:
            self.display_line1(text)

    def _set_brightness(self, level=140):
        self.lcd.write(chr(124))
        self.lcd.write(chr(level))

    def backlight(self, on=True):
        self.lcd.write(chr(12 if on else 8))

    def _set_position(self, position):
        starting = (128 + 64 + position - 16) if position > 15 else 128 + position 
        self.lcd.write(chr(254))
        self.lcd.write(chr(starting))
